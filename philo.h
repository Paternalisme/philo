/*
** philo.h for philo in /home/barbis_j/Documents/Projets/philo
** 
** Made by Joseph Barbisan
** Login   <barbis_j@epitech.net>
** 
** Started on  Fri Feb 20 16:16:15 2015 Joseph Barbisan
** Last update Sat Feb 28 13:19:49 2015 calvez kevin
*/

#ifndef PHILO_H_
# define PHILO_H_

# define SLEEP_TIME 1
# define EAT_TIME 2
# define THINK_TIME 3

int		prev_philo(int philo);
int		next_philo(int philo);
void		refresh_philo(int ***tab, int philo, int left, int right);
int		available_cstick(int ***tab, int philo);
int		available_cstick_sleep(int ***tab, int philo);

int		take_cstick(int id, int status);
int		**fill_tab(int **tab);
void		*philo(void *arg);
#endif
