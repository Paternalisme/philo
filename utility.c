/*
** get_nbstick.c for  in /home/calvez_k/rendu/philo/philo
** 
** Made by calvez kevin
** Login   <calvez_k@epitech.net>
** 
** Started on  Mon Feb 23 14:20:47 2015 calvez kevin
** Last update Mon Feb 23 20:41:59 2015 calvez kevin
*/

int		prev_philo(int philo)
{
  if (philo == 0)
    return (6);
  return (philo - 1);
}

int		next_philo(int philo)
{
  if (philo == 6)
    return (0);
  return (philo + 1);
}

void		refresh_philo(int ***tab, int philo, int left, int right)
{
  if (left == 1)
    {
      (*tab)[prev_philo(philo)][1] = 0;
      (*tab)[philo][0] = 1;
    }
  if (right == 1)
    {
      (*tab)[next_philo(philo)][0] = 0;
      (*tab)[philo][1] = 1;
    }
}

int		available_cstick(int ***tab, int philo)
{
  int		left;
  int		right;

  left = 1;
  if ((*tab)[prev_philo(philo)][0] == 1)
    left = 0;
  right = 1;
  if ((*tab)[next_philo(philo)][1] == 1)
    right = 0;
  refresh_philo(tab, philo, left, right);
  return (left + right);
}

int		available_cstick_sleep(int ***tab, int philo)
{
  int		left;
  int		right;

  left = 1;
  if ((*tab)[prev_philo(philo)][1] == 1 && (*tab)[prev_philo(philo)][0] == 1)
    left = 0;
  right = 1;
  if ((*tab)[next_philo(philo)][1] == 1 && (*tab)[next_philo(philo)][0] == 1)
    right = 0;
  refresh_philo(tab, philo, left, right);
  return (left + right);
}
