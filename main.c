/*
** main.c for philo in /home/barbis_j/Documents/Projets/philo
** 
** Made by Joseph Barbisan
** Login   <barbis_j@epitech.net>
** 
** Started on  Thu Feb 19 15:50:49 2015 Joseph Barbisan
** Last update Fri Feb 27 18:13:36 2015 calvez kevin
*/

#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "philo.h"

void		philo_sleep(int status, int id)
{
  if (status == 2)
    {
      printf("PHILOSOPHE %d : EATING\n", id);
      sleep(2);
    }
  else if (status == 1)
    {	
      printf("PHILOSOPHE %d : THINKING\n", id);
      sleep(3);
    }
  else if (status == 0)
    {
      printf("PHILOSOPHE %d : SLEEPING\n", id);
      sleep(3);
    }
}

void		*philo(void *arg)
{
  int		id;
  int		status;
  int		new_status;

  id = *(int *)arg;
  status = -1;
  while (1)
    {
      if ((new_status = take_cstick(id, status)) == -1)
	exit(0);
      while (status == 1 && new_status != 2)
	{
	  if ((new_status = take_cstick(id, status)) == -1)
	    exit(0);
	  usleep(50);
	}
      while (status == 2 && new_status == 2)
	{
	  if ((new_status = take_cstick(id, status)) == -1)
	    exit(0);
	  usleep(50);
	}
      philo_sleep(new_status, id);
      status = new_status;
    }
}

int		**fill_tab(int **tab)
{
  int		i;

  i = 0;
  if ((tab = malloc(7 * sizeof(*tab))) == NULL)
    return (NULL);
  while (i < 7)
    {
      if ((tab[i] = malloc(2 * sizeof(**tab))) == NULL)
	return (NULL);
      ++i;
    }
  i = 0;
  while (i < 7)
    {
      tab[i][0] = 0;
      tab[i][0] = 1;
      ++i;
    }
  return (tab);
}

int				take_cstick(int id, int status)
{
  static int			**philo_tab = NULL;
  static pthread_mutex_t	mutex = PTHREAD_MUTEX_INITIALIZER;
  int				ret;

  if (pthread_mutex_lock(&mutex) != 0)
    return (-1);
  if (philo_tab == NULL)
    if ((philo_tab = fill_tab(philo_tab)) == NULL)
      return (-1);
  philo_tab[id][0] = 0;
  philo_tab[id][1] = 0;
  if (pthread_mutex_unlock(&mutex) != 0)
    return (-1);
  usleep(500);
  if (pthread_mutex_lock(&mutex) != 0)
    return (-1);
  //if (status == 0)
  //  ret = available_cstick_sleep(&philo_tab, id);
  //else
  ret = available_cstick(&philo_tab, id);
  if (pthread_mutex_unlock(&mutex) != 0)
    return (-1);
  return (ret);
}

int		main()
{
  pthread_t	p[7];
  int		tab[7];
  int		i;

  i = 0;
  while (i < 7)
    {
      tab[i] = i;
      if (pthread_create(&p[i], NULL, philo, &tab[i]) != 0)
	return (-1);
      ++i;
    }
  sleep(10);
  i = 0;
  while (i < 7)
    {
      if (pthread_join(p[i], NULL) != 0)
	return (0);
      ++i;
    }
  return (0);
}
