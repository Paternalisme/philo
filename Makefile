##
## Makefile for  in /home/calvez_k
## 
## Made by calvez kevin
## Login   <calvez_k@epitech.net>
## 
## Started on  Fri Feb 20 16:18:59 2015 calvez kevin
## Last update Tue Feb 24 14:06:28 2015 Joseph Barbisan
##

CC	= gcc

RM	= rm -f

SRC	= main.c \
	  utility.c

NAME	= philo

OBJ	= $(SRC:.c=.o)

CFLAGS	+= -Wextra -Wall -g


all:	$(NAME)

$(NAME):$(OBJ)
	$(CC) -o $(NAME) $(OBJ) -lpthread

clean:
	$(RM) $(OBJ)

fclean:	clean
	$(RM) $(NAME)

re:	fclean all

.PHONY:	re clean fclean all
